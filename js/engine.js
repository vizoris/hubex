
$(function() {

var mapSlider = $('.map-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 3,
        }
        },
        {
          breakpoint: 991,
          settings: {
              slidesToShow: 2,

            }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            },
        
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1,

            },
        
        }
    ]
});
$('.map-slider__prev').click(function(){
  $(mapSlider).slick("slickNext");
 });
 $('.map-slider__next').click(function(){
  $(mapSlider).slick("slickPrev");
 });


var reviewsSlider =  $('.reviews-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: false,

 });
$('.reviews-slider__prev').click(function(){
  $(reviewsSlider).slick("slickNext");
 });
 $('.reviews-slider__next').click(function(){
  $(reviewsSlider).slick("slickPrev");
 });


var missionSlider =  $('.mission-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: false,

 });
 $('.mission-slider__next').click(function(){
  $(missionSlider).slick("slickNext");
 });




$('.partners-slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [{
        breakpoint: 700,
        settings: {
            slidesToShow: 1,
        }
        },
    ]

 });



var featuresSlider =  $('.features-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: false,
     responsive: [{
        breakpoint: 991,
        settings: {
            slidesToShow: 3,
        }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            }
        },
        {
          breakpoint: 550,
          settings: {
              slidesToShow: 1,

            },
        
        },

    ]

 });
$('.features-slider__prev').click(function(){
  $(featuresSlider).slick("slickNext");
 });
 $('.features-slider__next').click(function(){
  $(featuresSlider).slick("slickPrev");
 });


var informationSlider =  $('.information-slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: false,
    responsive: [{
        breakpoint: 991,
        settings: {
            slidesToShow: 1,
        }
        },
    ]

 });
$('.information-slider__prev').click(function(){
  $(informationSlider).slick("slickNext");
 });
 $('.information-slider__next').click(function(){
  $(informationSlider).slick("slickPrev");
 });




// Top Slider
$('.top-slider__text').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 4,
        }
        },
        {
          breakpoint: 991,
          settings: {
              slidesToShow: 3,

            }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            },
        
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1,

            },
        
        }
    ]
});


$('.top-slider__icon').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 4,
        }
        },
        {
          breakpoint: 991,
          settings: {
              slidesToShow: 3,

            }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            },
        
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1,

            },
        
        }
    ]
});


$(".opportunity-menu__burger").click(function(){
  $(this).toggleClass("active");
  $('.opportunity-menu').toggleClass('active');
});



$('#app-slider').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed:5000,
    fade: false,
    arrows: true,
    appendArrows: $('#app-slider__arrows'),
    prevArrow: '<div class="service-arrow service-arrow__prev"><i class="icon-prev-ic"></i></div>',
    nextArrow: '<div class="service-arrow service-arrow__next"><i class="icon-next-ic"></i></div>',
 dots: true,
 appendDots: $('#app-slider__dots'),
     customPaging : function(slider, i) {
var thumb = $(slider.$slides[i]).data();

  return '0' + (i + 1);
        },
 dotsClass: 'service-dots__list',

});


$('#service-slider-2').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed:5000,
    fade: false,
    arrows: true,
    appendArrows: $('#service-arrows-2'),
    prevArrow: '<div class="service-arrow service-arrow__prev"><i class="icon-prev-ic"></i></div>',
    nextArrow: '<div class="service-arrow service-arrow__next"><i class="icon-next-ic"></i></div>',
 dots: true,
 appendDots: $('#service-dots-2'),
     customPaging : function(slider, i) {
var thumb = $(slider.$slides[i]).data();

  return '0' + (i + 1);
        },
 dotsClass: 'service-dots__list',

});



if ($(window).width() < 768) {
  $('.service-item__block--text .h4').click(function() {
    $(this).parent().toggleClass('active');
    $(this).next('p').fadeToggle();
  })
}



$('#practice-slider-1').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed:5000,
    fade: false,
    arrows: true,
    appendArrows: $('#service-arrows-3'),
    prevArrow: '<div class="service-arrow service-arrow__prev"><i class="icon-prev-ic"></i></div>',
    nextArrow: '<div class="service-arrow service-arrow__next"><i class="icon-next-ic"></i></div>',
 dots: true,
 appendDots: $('#service-dots-3'),
     customPaging : function(slider, i) {
var thumb = $(slider.$slides[i]).data();

  return '0' + (i + 1);
        },
 dotsClass: 'service-dots__list',
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 3,
        }
        },
        {
          breakpoint: 991,
          settings: {
              slidesToShow: 3,

            }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            },
        
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1,

            },
        
        }
    ]
});


$('#materials-slider').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed:5000,
    fade: false,
    arrows: true,
    appendArrows: $('#service-arrows-4'),
    prevArrow: '<div class="service-arrow service-arrow__prev"><i class="icon-prev-ic"></i></div>',
    nextArrow: '<div class="service-arrow service-arrow__next"><i class="icon-next-ic"></i></div>',
 dots: true,
 appendDots: $('#service-dots-4'),
     customPaging : function(slider, i) {
var thumb = $(slider.$slides[i]).data();

  return '0' + (i + 1);
        },
 dotsClass: 'service-dots__list',
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 3,
        }
        },
        {
          breakpoint: 991,
          settings: {
              slidesToShow: 3,

            }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            }
        
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1,

            }
        
        }
    ]
});



$('#news-slider').slick({
    infinite: true,
    speed: 600,
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed:5000,
    fade: false,
    arrows: true,
    appendArrows: $('#service-arrows-5'),
    prevArrow: '<div class="service-arrow service-arrow__prev"><i class="icon-prev-ic"></i></div>',
    nextArrow: '<div class="service-arrow service-arrow__next"><i class="icon-next-ic"></i></div>',
 dots: true,
 appendDots: $('#service-dots-5'),
     customPaging : function(slider, i) {
var thumb = $(slider.$slides[i]).data();

  return '0' + (i + 1);
        },
 dotsClass: 'service-dots__list',
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 3,
        }
        },
        {
          breakpoint: 991,
          settings: {
              slidesToShow: 3,

            }
        },
        {
          breakpoint: 768,
          settings: {
              slidesToShow: 2,

            }
        
        },
        {
          breakpoint: 479,
          settings: {
              slidesToShow: 1,

            }
        
        }
    ]
});



// BEGIN of script WOW.
new WOW().init();


// Main-menu show
$('.burger-menu').click(function() {
  $(this).toggleClass('active');
  $('.main-menu').toggleClass('active');
})




// components-block slider

if ($(window).width() < 1200) {
  
  $('.components-block').slick({
  dots: true,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  adaptiveHeight: true,
  // autoplay: true,
  // autoplaySpeed: 5000,

});

}




// works slider

if ($(window).width() < 768) {
  
  $('.work-wrap').slick({
  dots: true,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
   adaptiveHeight: true,
  // autoplay: true,
  // autoplaySpeed: 5000,

});

}



// FansyBox
 $('.fancybox').fancybox({});





$('.product-slider').slick({
  dots: true,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,

});


// Фиксированное меню
if ($(window).width() > 1259) {
 window.onload = function() { 
      $('.airSticky').airStickyBlock();
  };
};


// Показать/скрыть блок Интересно

$('.interesting-more').click(function() {
  $(this).toggleClass('active');
  $('.interesting-body').slideToggle()
})



 // Туллтип
 if ($(window).width() >= 1200) {

  $('.tooltip-icon').hover(function() {
    $(this).next('.tooltip-text').stop(true, true).fadeIn();
  }, function() {
    $('.tooltip-text').fadeOut();
  }
  )


}else
{
  $('.tooltip-icon').click(function() {
    $(this).next('.tooltip-text').fadeIn();
  })
  $('.tooltip-close').click(function() {
    $('.tooltip-text').fadeOut();
  })


}


 // Стилизация селектов
$('select').styler();




// Скролл

$(".scroll").click(function(event){
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 20}, 1500);
  });








})